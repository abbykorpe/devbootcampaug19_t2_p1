package com.tw.bootcamp.evolutionoftrust;

import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;
import java.util.Observable;
import java.util.Observer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;

public class CopyCatPlayerTest {
    @Test
    public void ShouldHaveCopyCatInstance(){
        Player copyCatPlayer = new CopyCatPlayer("player1","player2");
        Assert.assertNotNull(copyCatPlayer);
    }

    @Test
    public void shouldCreateAnObservableCopyCatInstance() {
        CopyCatPlayer copyCatPlayer = new CopyCatPlayer("player1","player2");
        assertTrue(copyCatPlayer instanceof Observer);
    }

    @Test
    public void shouldReturnCoOperateAsFirstMove(){
        CopyCatPlayer copyCatPlayer = new CopyCatPlayer("player1","player2");
        assertEquals(Move.COOPERATE,copyCatPlayer.movePlayed());
    }

    @Test
    public void shouldUpdateTheMoveWhenObservableObjectPublishesAnUpdate(){
        CopyCatPlayer copyCatPlayer = new CopyCatPlayer("player1","player2");
        copyCatPlayer.update(
                mock(Observable.class),
                new HashMap<String,Move>(){{put("player2",Move.CHEAT);
                    put("player1",Move.COOPERATE);}}

                );
        assertEquals(Move.CHEAT, copyCatPlayer.movePlayed());
    }


}
