package com.tw.bootcamp.evolutionoftrust;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ConsolePlayerTest {

    private static ConsolePlayer player;
    private static ConsoleIO mockIO;

    @Before
    public void setup() {
        mockIO = mock(ConsoleIO.class);
        player = new ConsolePlayer("player1",0, mockIO);
    }

    @Test
    public void shouldReturnPlayersMove() {
        //given
        Move expected = Move.CHEAT;

        //when
        when(mockIO.nextLine()).thenReturn("1");
        Move actual = player.movePlayed();

        //then
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void shouldReturnPlayerCooperateMove() {
        //given
        Move expected = Move.COOPERATE;

        //when
        when(mockIO.nextLine()).thenReturn("2");
        Move actual = player.movePlayed();

        //then
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void shouldUpdatePlayerScore() {
        //given
        int updatedScore = 5;
        int expected = 5;

        //when
        player.updateScore(5);
        int updated = player.score();

        //then
        Assert.assertEquals(expected, updated);
    }

}