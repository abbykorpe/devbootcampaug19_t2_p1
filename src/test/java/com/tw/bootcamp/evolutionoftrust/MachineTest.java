package com.tw.bootcamp.evolutionoftrust;


import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class MachineTest {

    private static Machine machine;

    @Before
    public void setUp() {
        machine = new Machine();
    }

    @Test
    public void shouldReturnScoreWhenBothPlayersCooperateForInitialRound() {
        //given
        Scores intialScores = new Scores(0, 0);
        Move player1Move = Move.COOPERATE;
        Move player2Move = Move.COOPERATE;
        Scores expected = new Scores(2, 2);

        //when
        Scores actual = machine.calculateScore(intialScores, player1Move, player2Move);

        //then
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void shouldReturnUpdatedScoresWhenBothPlayersCooperateForRoundTwo() {
        //given
        Scores intialScores = new Scores(1, 0);
        Move player1Move = Move.COOPERATE;
        Move player2Move = Move.COOPERATE;
        Scores expected = new Scores(3, 2);

        //when
        Scores actual = machine.calculateScore(intialScores, player1Move, player2Move);

        //then
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void shouldReturnUpdatedScoresWhenBothPlayersCheatForInitialRound() {
        //given
        Scores intialScores = new Scores(0, 0);
        Move player1Move = Move.CHEAT;
        Move player2Move = Move.CHEAT;
        Scores expected = new Scores(0, 0);

        //when
        Scores actual = machine.calculateScore(intialScores, player1Move, player2Move);

        //then
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void shouldReturnUpdatedScoresWhenBothPlayersCheatForRoundTwo() {
        //given
        Scores intialScores = new Scores(1, 0);
        Move player1Move = Move.CHEAT;
        Move player2Move = Move.CHEAT;
        Scores expected = new Scores(1, 0);

        //when
        Scores actual = machine.calculateScore(intialScores, player1Move, player2Move);

        //then
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void shouldReturnUpdatedScoresWhenBothPlayersMakeDifferentMovesForInitialRound() {
        //given
        Scores intialScores = new Scores(0, 0);
        Move player1Move = Move.COOPERATE;
        Move player2Move = Move.CHEAT;
        Scores expected = new Scores(-1, 3);

        //when
        Scores actual = machine.calculateScore(intialScores, player1Move, player2Move);

        //then
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void shouldReturnUpdatedScoresWhenBothPlayersMakeDifferentMovesForRoundTwo() {
        //given
        Scores intialScores = new Scores(1, 0);
        Move player1Move = Move.COOPERATE;
        Move player2Move = Move.CHEAT;
        Scores expected = new Scores(0, 3);

        //when
        Scores actual = machine.calculateScore(intialScores, player1Move, player2Move);

        //then
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void shouldReturnUpdatedScoresWhenPlayersMakeDifferentMovesForInitialRound() {
        //given
        Scores intialScores = new Scores(0, 0);
        Move player1Move = Move.CHEAT;
        Move player2Move = Move.COOPERATE;
        Scores expected = new Scores(3, -1);

        //when
        Scores actual = machine.calculateScore(intialScores, player1Move, player2Move);

        //then
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void shouldReturnUpdatedScoresWhenPlayersMakeDifferentMovesForRoundTwo() {
        //given
        Scores intialScores = new Scores(1, 0);
        Move player1Move = Move.CHEAT;
        Move player2Move = Move.COOPERATE;
        Scores expected = new Scores(4, -1);

        //when
        Scores actual = machine.calculateScore(intialScores, player1Move, player2Move);

        //then
        Assert.assertEquals(expected, actual);
    }

}