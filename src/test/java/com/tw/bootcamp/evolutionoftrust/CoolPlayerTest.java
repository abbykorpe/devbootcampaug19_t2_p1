package com.tw.bootcamp.evolutionoftrust;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CoolPlayerTest {

    private static Player coolPlayer;

    @Before
    public void setup() {
        coolPlayer = new CoolPlayer("player1",0);
    }

    @Test
    public void itShouldReturnCooperateMove() {
        //given
        Move expected = Move.COOPERATE;

        //when
        Move actual = coolPlayer.movePlayed();

        //then
        assertEquals(expected, actual);
    }

    @Test
    public void itShouldAlwaysReturnCooperateMove(){
        //given
        Move expected = Move.COOPERATE;

        //when
        Move firstMove = coolPlayer.movePlayed();
        Move secondMove = coolPlayer.movePlayed();
        Move thirdMove = coolPlayer.movePlayed();
        Move fourthMove = coolPlayer.movePlayed();

        //then
        assertEquals(expected, firstMove);
        assertEquals(expected, secondMove);
        assertEquals(expected, thirdMove);
        assertEquals(expected, fourthMove);
    }
}
