package com.tw.bootcamp.evolutionoftrust;

import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.Observable;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class GameTest {

    @Test
    public void shouldReturnGameScoreForEachPlayerAfterFirstRound() {
        //given
        ConsolePlayer player1 = mock(ConsolePlayer.class);
        ConsolePlayer player2 = mock(ConsolePlayer.class);
        Machine machine = mock(Machine.class);
        ConsoleIO console = mock(ConsoleIO.class);
        int numberOfRounds = 1;
        Game game = new Game(player1, player2, machine, console, numberOfRounds);
        Move cheatMove = Move.CHEAT;
        Move cooperateMove = Move.COOPERATE;
        Scores initialScore = new Scores(0, 0);

        //when
        when(player1.movePlayed()).thenReturn(cheatMove);
        when(player2.movePlayed()).thenReturn(cooperateMove);
        when(machine.calculateScore(any(Scores.class), any(Move.class), any(Move.class))).thenReturn(new Scores(3, -1));
        game.play();

        //then
        verify(player1).movePlayed();
        verify(player2).movePlayed();
        verify(machine).calculateScore(initialScore, cheatMove, cooperateMove);
        verify(console).printLine("Scores: 3 -1");
    }

    @Test
    public void shouldReturnGameScoreForEachPlayerAfterTwoRounds() {
        //given
        ConsolePlayer player1 = mock(ConsolePlayer.class);
        ConsolePlayer player2 = mock(ConsolePlayer.class);
        Machine machine = mock(Machine.class);
        ConsoleIO console = mock(ConsoleIO.class);
        int numberOfRounds = 2;
        Game game = new Game(player1, player2, machine, console, numberOfRounds);
        Move cheatMove = Move.CHEAT;
        Move cooperateMove = Move.COOPERATE;
        Scores initialScore = new Scores(0, 0);
        Scores scoreAfterRoundOne = new Scores(3, -1);

        //when
        when(player1.movePlayed()).thenReturn(cheatMove).thenReturn(cooperateMove);
        when(player2.movePlayed()).thenReturn(cooperateMove).thenReturn(cheatMove);
        when(machine.calculateScore(any(Scores.class), any(Move.class), any(Move.class)))
                .thenReturn(new Scores(3, -1))
                .thenReturn(new Scores(2, 2));
        game.play();

        //then
        verify(player1, times(2)).movePlayed();
        verify(player2, times(2)).movePlayed();
        verify(machine).calculateScore(initialScore, cheatMove, cooperateMove);
        verify(machine).calculateScore(scoreAfterRoundOne, cooperateMove, cheatMove);
        verify(console).printLine("Scores: 3 -1");
        verify(console).printLine("Scores: 2 2");
    }

    @Test
    public void shouldReturnGameScoreForOneCoolPlayerAfterTwoRounds() {
        //given
        Player player1 = mock(CoolPlayer.class);
        Player player2 = mock(ConsolePlayer.class);
        Machine machine = mock(Machine.class);
        ConsoleIO console = mock(ConsoleIO.class);
        int numberOfRounds = 2;
        Game game = new Game(player1, player2, machine, console, numberOfRounds);
        Move cheatMove = Move.CHEAT;
        Move cooperateMove = Move.COOPERATE;
        Scores initialScore = new Scores(0, 0);
        Scores scoreAfterRoundOne = new Scores(2, 2);

        //when
        when(player1.movePlayed()).thenReturn(cooperateMove).thenReturn(cooperateMove);
        when(player2.movePlayed()).thenReturn(cooperateMove).thenReturn(cheatMove);
        when(machine.calculateScore(any(Scores.class), any(Move.class), any(Move.class)))
                .thenReturn(new Scores(2, 2))
                .thenReturn(new Scores(1, 5));
        game.play();

        //then
        verify(player1, times(2)).movePlayed();
        verify(player2, times(2)).movePlayed();
        verify(machine).calculateScore(initialScore, cooperateMove, cooperateMove);
        verify(machine).calculateScore(scoreAfterRoundOne, cooperateMove, cheatMove);
        verify(console).printLine("Scores: 2 2");
        verify(console).printLine("Scores: 1 5");
    }

    @Test
    public void shouldBeAnInstanceOfObservable(){
        Player player1 = mock(CoolPlayer.class);
        Player player2 = mock(CopyCatPlayer.class);
        Machine machine = mock(Machine.class);
        ConsoleIO console = mock(ConsoleIO.class);
        int numberOfRounds = 2;
        Game game = new Game(player1, player2, machine, console, numberOfRounds);
        Assert.assertTrue(game instanceof Observable);
    }

    @Test
    public void shouldAddCopyCatPlayerAsObservers() {
        Player player1 = mock(CoolPlayer.class);
        CopyCatPlayer player2 = mock(CopyCatPlayer.class);
        Machine machine = mock(Machine.class);
        ConsoleIO console = mock(ConsoleIO.class);
        int numberOfRounds = 1;
        Game game = new Game(player1, player2, machine, console, numberOfRounds);
        game.addObserver(player2);
        assertEquals(1, game.countObservers());
    }

    @Test
    public void shouldUpdateCopyCatPlayerWithOpponentsLastMove() {
        int numberOfRounds = 2;
        ConsolePlayer player1 = mock(ConsolePlayer.class);
        CopyCatPlayer player2 = mock(CopyCatPlayer.class);
        Machine machine = mock(Machine.class);
        ConsoleIO console = mock(ConsoleIO.class);
        Scores scores = mock(Scores.class);
        when(player1.movePlayed()).thenReturn(Move.CHEAT, Move.COOPERATE);
        when(player2.movePlayed()).thenReturn(Move.COOPERATE, Move.CHEAT);
        player1.name="player1";
        player2.name="player2";
//      when(player1.getName()).thenReturn("player1");
//        when(player2.getName()).thenReturn("player2");
        when(scores.getPlayer1Score()).thenReturn(3).thenReturn(-1);
        when(scores.getPlayer2Score()).thenReturn(-1).thenReturn(3);
        when(machine.calculateScore(any(Scores.class), any(Move.class), any(Move.class))).thenReturn(scores);

        Game game = new Game(player1, player2, machine, console, numberOfRounds);
        game.addObserver(player2);
        game.play();
        //assertEquals(1, game.countObservers());
        Map <String,Move> map =new HashMap<String,Move>(){{put("player2",Move.CHEAT);
            put("player1",Move.COOPERATE);}};
        verify(player2, times(2)).update(game, map);

    }

    @Test
    public  void shouldUpdateGrudgePlayerWithLastMove() {

        int numberOfRounds = 2;
        Player player1 = mock(ConsolePlayer.class);
        GrudgerPlayer player2 = mock(GrudgerPlayer.class);
        Machine machine = mock(Machine.class);
        ConsoleIO console = mock(ConsoleIO.class);
        Scores scores = mock(Scores.class);
        when(player1.movePlayed()).thenReturn(Move.CHEAT, Move.COOPERATE);
        when(player2.movePlayed()).thenReturn(Move.COOPERATE, Move.CHEAT);
        when(scores.getPlayer1Score()).thenReturn(3).thenReturn(-1);
        when(scores.getPlayer2Score()).thenReturn(-1).thenReturn(3);
        when(machine.calculateScore(any(Scores.class), any(Move.class), any(Move.class))).thenReturn(scores);
        player1.name="player1";
        player2.name="player2";
        Game game = new Game(player1, player2, machine, console, numberOfRounds);
        game.addObserver(player2);
        game.play();
        //assertEquals(1, game.countObservers());
        Map <String,Move> map =new HashMap<String,Move>(){{put("player2",Move.CHEAT);
            put("player1",Move.COOPERATE);}};
        verify(player2,times(2)).update(game,map);
    }
}
