package com.tw.bootcamp.evolutionoftrust;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CheatPlayerTest {

    private static Player cheatPlayer;

    @Before
    public void setup() {
        cheatPlayer = new CheatPlayer("player1",0);
    }

    @Test
    public void itShouldReturnCooperateMove() {
        //given
        Move expected = Move.CHEAT;

        //when
        Move actual = cheatPlayer.movePlayed();

        //then
        assertEquals(expected, actual);
    }

    @Test
    public void itShouldAlwaysReturnCooperateMove(){
        //given
        Move expected = Move.CHEAT;

        //when
        Move firstMove = cheatPlayer.movePlayed();
        Move secondMove = cheatPlayer.movePlayed();
        Move thirdMove = cheatPlayer.movePlayed();
        Move fourthMove = cheatPlayer.movePlayed();

        //then
        assertEquals(expected, firstMove);
        assertEquals(expected, secondMove);
        assertEquals(expected, thirdMove);
        assertEquals(expected, fourthMove);
    }
}
