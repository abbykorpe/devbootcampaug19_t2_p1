package com.tw.bootcamp.evolutionoftrust;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class IntegrationTest {

    @Test
    public void shouldSimulateGameForOneCoolAndOneCheatPlayer() {
        //given
        Player player1 = new CoolPlayer("player1",0);
        Player player2 = new CheatPlayer("player2",0);
        Machine machine = new Machine();
        ConsoleIO console = mock(ConsoleIO.class);
        int numberOfRounds = 2;
        Game game = new Game(player1, player2, machine, console, numberOfRounds);

        //when
        game.play();

        //then
        verify(console).printLine("Scores: -1 3");
        verify(console).printLine("Scores: -2 6");
        assertEquals(-2, player1.score);
        assertEquals(6, player2.score);
    }

    @Test
    public void shouldSimulateGameForOneCoolPlayerAndOneConsolePlayer() {
        //given
        Player player1 = new CoolPlayer("player1",0);
        ConsoleIO console = mock(ConsoleIO.class);
        Player player2 = new ConsolePlayer("player2",0, console);
        Machine machine = new Machine();
        int numberOfRounds = 2;
        Game game = new Game(player1, player2, machine, console, numberOfRounds);

        //when
        when(console.nextLine()).thenReturn("1");
        when(console.nextLine()).thenReturn("1");
        game.play();

        //then
        verify(console).printLine("Scores: -1 3");
        verify(console).printLine("Scores: -2 6");
        assertEquals(-2, player1.score);
        assertEquals(6, player2.score);
    }

    @Test
    public void shouldSimulateGameForOneCoolPlayerAndOneConsolePlayerWithDifferentInitialScores() {
        //given
        Player player1 = new CoolPlayer("player1",4);
        ConsoleIO console = mock(ConsoleIO.class);
        Player player2 = new ConsolePlayer("player2",5, console);
        Machine machine = new Machine();
        int numberOfRounds = 2;
        Game game = new Game(player1, player2, machine, console, numberOfRounds);

        //when
        when(console.nextLine()).thenReturn("1");
        when(console.nextLine()).thenReturn("1");
        game.play();

        //then
        verify(console).printLine("Scores: 3 8");
        verify(console).printLine("Scores: 2 11");
        assertEquals(2, player1.score);
        assertEquals(11, player2.score);
    }


}
