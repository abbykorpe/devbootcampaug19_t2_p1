package com.tw.bootcamp.evolutionoftrust;

import  org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Observable;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;

public class GrudgerPlayerTest {


    GrudgerPlayer grudgerPlayer;
    @Before
    public  void initGradugePlayer() {
        grudgerPlayer = new GrudgerPlayer("player1","player2");
    }
    @Test
    public void shouldHaveGrudgerPlayerInstance(){
        assertNotNull(grudgerPlayer);
    }

    @Test
    public void shouldReturnCooperateOnFirstMove() {
        //assertEquals();
        assertEquals(Move.COOPERATE,grudgerPlayer.movePlayed()) ;
    }

    @Test
    public void shouldReturnCheatWhenMoveIsCheat(){
        grudgerPlayer.update(mock(Observable.class),Move.CHEAT);
        assertEquals(Move.CHEAT, grudgerPlayer.movePlayed());
    }

    @Test
    public  void shouldAlwaysReturnCheatIfCheated() {
        grudgerPlayer.update(mock(Observable.class),Move.CHEAT);
        assertEquals(Move.CHEAT, grudgerPlayer.movePlayed());

        grudgerPlayer.update(mock(Observable.class),Move.COOPERATE);
        assertEquals(Move.CHEAT, grudgerPlayer.movePlayed());
    }
}

