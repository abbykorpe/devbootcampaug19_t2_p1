package com.tw.bootcamp.evolutionoftrust;

public class ConsolePlayer extends Player {

    private ConsoleIO consoleIO;

    public ConsolePlayer(String name, int score, ConsoleIO consoleIO) {
        super(name);
        this.consoleIO = consoleIO;
        this.score = score;
    }

    public Move movePlayed() {
        String input = consoleIO.nextLine();
        if (input.equals("1")) return Move.CHEAT;
        return Move.COOPERATE;
    }
    public String getName() {
        return this.name;
    }

}