package com.tw.bootcamp.evolutionoftrust;

import java.util.HashMap;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

public class CopyCatPlayer extends Player implements Observer {

    private final String opponentName;
    private Move move;

    public CopyCatPlayer(String name, String opponentName){
        super(name);
        this.opponentName = opponentName;
    }

    @Override
    public Move movePlayed() {
        if(move == null) {
            return Move.COOPERATE;
        }
        return this.move;
    }


    @Override
    public void update(Observable o, Object moveobj) {


        Map<String,Move> playermap = (HashMap<String,Move>)moveobj;
         this.move=playermap.get(this.opponentName);
    }

    public String getName() {
        return this.name;
    }
}
