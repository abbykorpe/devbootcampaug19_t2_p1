package com.tw.bootcamp.evolutionoftrust;

import java.util.function.BiFunction;

public class Machine {

    private static final int BOTH_COOPERATION_SCORE = 2;
    private static final int CHEAT_SCORE = -1;
    private static final int COOPERATION_SCORE = 3;

    public Scores calculateScore(Scores initialScores, Move player1Move, Move player2Move) {
        Scores updatedScores = calculateScores.apply(player1Move, player2Move);
        return updateScores.apply(updatedScores, initialScores);
    }

    private BiFunction<Move, Move, Scores> calculateScores = (player1Move, player2Move) -> {
        if (player1Move == Move.CHEAT && player2Move == Move.CHEAT) {
            return new Scores(0, 0);
        } else if (player1Move == Move.COOPERATE && player2Move == Move.CHEAT) {
            return new Scores(CHEAT_SCORE, COOPERATION_SCORE);
        } else if (player1Move == Move.CHEAT && player2Move == Move.COOPERATE) {
            return new Scores(COOPERATION_SCORE, CHEAT_SCORE);
        }
        return new Scores(BOTH_COOPERATION_SCORE, BOTH_COOPERATION_SCORE);
    };

    private BiFunction<Scores, Scores, Scores> updateScores =
            (updatedScores, initialScores) ->
                    new Scores(
                            updatedScores.getPlayer1Score() + initialScores.getPlayer1Score(),
                            updatedScores.getPlayer2Score() + initialScores.getPlayer2Score()
                    );

}