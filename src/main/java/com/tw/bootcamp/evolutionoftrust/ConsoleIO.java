package com.tw.bootcamp.evolutionoftrust;

import java.util.Scanner;

public class ConsoleIO {

    public String nextLine() {
        Scanner sc = new Scanner(System.in);
        return sc.nextLine();

    }

    public void printLine(String line) {
        System.out.println(line);
    }

}
