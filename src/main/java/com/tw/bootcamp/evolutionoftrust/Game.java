package com.tw.bootcamp.evolutionoftrust;

import java.util.HashMap;
import java.util.Map;
import java.util.Observable;

public class Game extends Observable {

    private final Player player1;
    private final Player player2;
    private final Machine machine;
    private final ConsoleIO console;
    private int numberOfRounds;

    public Game(Player player1, Player player2, Machine machine, ConsoleIO console, int numberOfRounds) {
        this.player1 = player1;
        this.player2 = player2;
        this.machine = machine;
        this.console = console;
        this.numberOfRounds = numberOfRounds;
    }


    private boolean isCopyCatPlayer(Player player) {
        return player instanceof CopyCatPlayer;
    }

    public void play() {
        Map<String,Move> playermap =new HashMap<>();
        Scores initialScore = new Scores(player1.score, player2.score);
        Scores updatedScores = initialScore;
        for (int round = 0; round < numberOfRounds; round++) {
            Move player1Move = player1.movePlayed();
            Move player2Move = player2.movePlayed();
            updatedScores = round == 0 ?
                    machine.calculateScore(initialScore, player1Move, player2Move) :
                    machine.calculateScore(updatedScores, player1Move, player2Move);
            player1.updateScore(updatedScores.getPlayer1Score());
            player2.updateScore(updatedScores.getPlayer2Score());
            playermap.put(player1.name,player1Move);
            playermap.put(player2.name,player2Move);
            setChanged();
            notifyObservers(playermap);
            /*notifyIfCopyCat(player1,player2Move);
            notifyIfCopyCat(player2,player1Move);*/

            console.printLine(updatedScores.toString());
        }
    }

//    private void notifyIfCopyCat(Player player,Move move) {
//        //if(isCopyCatPlayer(player)) {
//            setChanged();
//            notifyObservers(move);
//        //}
//    }
}