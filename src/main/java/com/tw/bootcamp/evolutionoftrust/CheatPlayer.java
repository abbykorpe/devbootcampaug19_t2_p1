package com.tw.bootcamp.evolutionoftrust;

public class CheatPlayer extends Player {

    public CheatPlayer(String name, int score) {
        super(name);
        this.score = score;
    }

    @Override
    public Move movePlayed() {
        return Move.CHEAT;
    }

}
