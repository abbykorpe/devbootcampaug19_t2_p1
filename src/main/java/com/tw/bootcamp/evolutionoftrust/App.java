package com.tw.bootcamp.evolutionoftrust;

public class App {

    public static void main(String[] args) {
        Player player1 = new CoolPlayer("player1",0);
        ConsoleIO console = new ConsoleIO();
        Player player2 = new ConsolePlayer("player2",0, console);
        Machine machine = new Machine();
        int numberOfRounds = 2;

        Game game = new Game(player1, player2, machine, console, numberOfRounds);

        game.play();
    }

}
