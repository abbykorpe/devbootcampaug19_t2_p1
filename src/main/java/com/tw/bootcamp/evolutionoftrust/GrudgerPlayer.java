package com.tw.bootcamp.evolutionoftrust;

import java.util.Observable;
import java.util.Observer;

public class GrudgerPlayer extends Player implements Observer {

    private String opponentName;

    public GrudgerPlayer(String name, String opponentName){
        super(name);
        this.opponentName = opponentName;
    }
    private  Move move =Move.COOPERATE;
    @Override
    public Move movePlayed() {
        return this.move;
    }

    @Override
    public void update(Observable o, Object move) {
        this.move= this.move != Move.CHEAT ? (Move) move : Move.CHEAT;

    }
}
