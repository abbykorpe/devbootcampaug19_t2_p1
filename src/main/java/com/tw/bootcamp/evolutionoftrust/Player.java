package com.tw.bootcamp.evolutionoftrust;

abstract class Player {
    int score;
    protected String name;

    public Player(String name){
        this.name = name;

    }
    public int score() {
        return this.score;
    }

    public void updateScore(int updatedScore) {
        this.score = updatedScore;
    }

    public abstract Move movePlayed();
}