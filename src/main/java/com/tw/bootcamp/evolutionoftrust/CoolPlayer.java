package com.tw.bootcamp.evolutionoftrust;

public class CoolPlayer extends Player {

    public CoolPlayer(String name, int score) {
        super(name);
        this.score = score;
    }

    @Override
    public Move movePlayed() {
        return Move.COOPERATE;
    }
}
