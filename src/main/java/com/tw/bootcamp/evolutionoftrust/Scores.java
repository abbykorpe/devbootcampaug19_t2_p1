package com.tw.bootcamp.evolutionoftrust;

public class Scores {

    private final int player1Score;
    private final int player2Score;

    public Scores(int player1Score, int player2Score) {
        this.player1Score = player1Score;
        this.player2Score = player2Score;
    }

    public int getPlayer1Score() {
        return player1Score;
    }

    public int getPlayer2Score() {
        return player2Score;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }
        Scores that = (Scores) obj;

        return that.player1Score == this.player1Score && that.player2Score == this.player2Score;
    }

    @Override
    public String toString() {
        return "Scores: " + getPlayer1Score() + " " + getPlayer2Score();
    }

}